GammaSPCe
=========

This Device Server controls a Gamma Vacuum Small Pump Controller (SPC).
It communicates with the controller over ethernet via a TCP socket.
    
How to run
----------
After installation, there is only one script: `GammaSPCe`.
You can use it via:
```console
GammaSPCe instance_name
```
Remember that Device Server instance has to registered in database previously.

Requirements
------------

- `setuptools`
- `pytango` >= 9.2.1

License
-------
This sample project is distributed under LGPLv3 (see `LICENSE` file).
