"""
This module contains metadata of GammaSPCe project
"""

__name__ = "dev-gammaspce"
__author__ = ""
__author_email__ = ""
__version__ = "1.2.0"
__license__ = "LGPLv3"
__url__ = "https://gitlab.m.cps.uj.edu.pl/CSIT/dev-maxiv-gammaspce.git"
__description__ = "Tango device server for SPCe ion pump controllers from Gamma Vacuum"
