import re
import time
from telnetlib import Telnet
from threading import Lock

# line feed character:
LF = "\r\n"
CHUNK_SIZE = 256


class SPCError(Exception):
    def __init__(self, msg=None, cmd=None):
        self.msg = msg
        self.cmd = cmd

    def __str__(self):
        s = self.msg
        if self.cmd:
            s += ' (command was "%s")' % self.cmd
        return s


class SPC(object):
    def __init__(self, host=None, port=23, timeout=1, cache_keeptime=1):
        self.timeout = timeout
        self.command_cache = {}
        self.cache_keeptime = cache_keeptime
        self.telnet = Telnet()
        # force telnetlib to use select() instead of poll()
        self.telnet._has_poll = False
        self.lock = Lock()
        if host:
            self.connect(host, port, timeout)

    def connect(self, host, port, timeout=1):
        with self.lock:
            self.telnet.open(host, port, timeout)

    def disconnect(self):
        with self.lock:
            self.telnet.close()
            self.command_cache = {}

    def command_inout(self, cmd):
        with self.lock:
            self.telnet.read_until(">", self.timeout)
            self.telnet.write(cmd + LF)
            result = self.telnet.read_until(LF, self.timeout)
            return result.strip(LF)

    def cached_command_inout(self, cmd):
        now = time.time()
        result, timestamp = self.command_cache.get(cmd, (None, 0))

        if now - timestamp > self.cache_keeptime:
            result = self.command_inout(cmd)
            timestamp = now
            self.command_cache[cmd] = (result, timestamp)

        return result

    def command_error(self, cmd, response):
        msg = 'Unexpected response from controller: "%s"' % response
        raise SPCError(msg, cmd)

    def info(self):
        return self.command_inout("info")

    def get_model_number(self):
        cmd = "SPC 01"
        r = self.cached_command_inout(cmd)
        m = re.match(r"OK \d\d ([\w\s]+)", r)
        if m is None:
            self.command_error(cmd, r)
        return m.group(1)

    def get_firmware_version(self):
        cmd = "SPC 02"
        r = self.cached_command_inout(cmd)
        m = re.match(r"OK \d\d FIRMWARE[\D:]*([\d\.]+)", r)
        if m is None:
            self.command_error(cmd, r)
        return m.group(1)

    def start_pump(self):
        cmd = "SPC 37"
        r = self.command_inout(cmd)
        m = re.match(r"OK \d\d", r)
        if m is None:
            self.command_error(cmd, r)

    def stop_pump(self):
        cmd = "SPC 38"
        r = self.command_inout(cmd)
        m = re.match(r"OK \d\d", r)
        if m is None:
            self.command_error(cmd, r)

    def get_status(self):
        cmd = "SPC 0D"
        r = self.cached_command_inout(cmd)
        m = re.match(r"OK \d\d ([\w\s]+)", r)
        if m is None:
            self.command_error(cmd, r)
        return m.group(1)

    def get_current(self):
        cmd = "SPC 0A"
        r = self.cached_command_inout(cmd)
        m = re.match(r"OK \d\d (\d.\d+E-?\d+) AMPS", r)
        if m is None:
            self.command_error(cmd, r)
        return float(m.group(1))

    def get_pressure(self):
        cmd = "SPC 0B"
        r = self.cached_command_inout(cmd)
        m = re.match(r"OK \d\d (\d.\d+E-?\d+) (Torr|MBA|PA)", r)
        if m is None:
            self.command_error(cmd, r)
        return float(m.group(1))

    def get_voltage(self):
        cmd = "SPC 0C"
        r = self.cached_command_inout(cmd)
        m = re.match(r"OK \d\d (-*\d+)", r)
        if m is None:
            self.command_error(cmd, r)
        return float(m.group(1))

    def get_high_voltage(self):
        cmd = "SPC 53"
        r = self.cached_command_inout(cmd)
        m = re.match(r"OK \d\d (-*\d+)", r)
        if m is None:
            self.command_error(cmd, r)
        return float(m.group(1))

    def set_high_voltage(self, voltage):
        cmd = "SPC 53 %d" % voltage
        r = self.command_inout(cmd)
        m = re.match(r"OK \d\d", r)
        if m is None:
            self.command_error(cmd, r)

    def get_pump_size(self):
        cmd = "SPC 11"
        r = self.cached_command_inout(cmd)
        m = re.match(r"OK \d\d (\d+)", r)
        if m is None:
            self.command_error(cmd, r)
        return float(m.group(1))

    def set_pump_size(self, size):
        cmd = "SPC 12 %d" % size
        r = self.command_inout(cmd)
        m = re.match(r"OK \d\d", r)
        if m is None:
            self.command_error(cmd, r)

    def set_pressure_units(self, units):
        cmd = "SPC 0E %s" % units.upper()
        r = self.command_inout(cmd)
        m = re.match(r"OK \d\d", r)
        if m is None:
            self.command_error(cmd, r)

    def get_setpoint(self):
        cmd = "SPC 3C"
        r = self.cached_command_inout(cmd)
        m = re.match(r"OK \d\d 1,([01]{1}),(\d.\d+e-?\d+),(\d.\d+e-?\d+),([01]{1})", r)
        if m is None:
            self.command_error(cmd, r)
        sp_enabled = m.group(1) == "1"
        on_pressure = float(m.group(2))
        off_pressure = float(m.group(3))
        sp_active = m.group(4) == "1"
        return sp_enabled, on_pressure, off_pressure, sp_active

    def set_setpoint(self, sp_enabled, on_pressure, off_pressure):
        cmd = "SPC 3D 1,%d,%1.2E,%1.2E" % (sp_enabled, on_pressure, off_pressure)
        r = self.command_inout(cmd)
        m = re.match(r"OK \d\d", r)
        if m is None:
            self.command_error(cmd, r)

    def get_tftp_server_ip(self):
        cmd = "SPC 4F"
        r = self.cached_command_inout(cmd)
        m = re.match(r"OK \d\d (\d+\.\d+.\d+.\d+)", r)
        if m is None:
            self.command_error(cmd, r)
        return m.group(1)

    def set_tftp_server_ip(self, address):
        cmd = "SPC 4F %s" % address
        r = self.command_inout(cmd)
        m = re.match(r"OK \d\d", r)
        if m is None:
            self.command_error(cmd, r)

    def set_firmware_update(self):
        cmd = "SPC 8F"
        r = self.command_inout(cmd)
        m = re.match(r"OK \d\d", r)
        if m is None:
            self.command_error(cmd, r)
