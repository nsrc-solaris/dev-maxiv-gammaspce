from facadedevice import Facade, proxy_attribute, proxy_command
from gammaspce.SPCLib import SPC
from tango import AttrWriteType, DebugIt, DevState, DispLevel
from tango.server import attribute, command, device_property


class GammaSPCe(Facade):
    pressure = 0
    current = 0
    voltage = 0
    high_voltage = 0.0
    pump_size = 0
    setpoint_on_pressure = 0
    setpoint_off_pressure = 0
    setpoint_enabled = False
    setpoint_active = False
    firmware_version = ""
    spc_model = ""
    spc_status = ""
    update_pending = False

    # timeouts in seconds
    connect_timeout_s = 3
    read_timeout_s = 3

    # spc telnet connection
    spc = None
    spc_error = None
    connected = False
    reconnect_time = 0

    def safe_init_device(self):
        super(GammaSPCe, self).safe_init_device()
        # timeouts in seconds
        self.connect_timeout_s = self.ConnectTimeout / 1000
        self.read_timeout_s = self.ReadTimeout / 1000

        # spc telnet connection
        self.spc = SPC(timeout=self.read_timeout_s)
        if not self.Host:
            self.error_stream("Host device property is not set")
        else:
            self.ReadValues()
        self.set_state(DevState.ON)
        self.set_status("The device is running.")

    # Properties
    Host = device_property(dtype=str, doc="Host name or IP address")

    Port = device_property(
        dtype=int,
        default_value=23,
        doc="Telnet port number where the controller is "
        "listening for incoming requests",
    )

    ReadTimeout = device_property(
        dtype=float, default_value=5000, doc="Read timeout in milliseconds"
    )

    ConnectTimeout = device_property(
        dtype=float, default_value=1000, doc="Connect timeout in milliseconds"
    )

    TFTPServerIP = device_property(
        dtype=str,
        default_value="192.168.130.90",
        doc="IP address for TFTP server hosting " "firmware images",
    )

    AlarmAttribute = device_property(dtype=str)

    BypassAttribute = device_property(dtype=str)

    # Attributes
    @attribute(
        dtype=float,
        unit="V",
        format="%6.2f",
        display_level=DispLevel.OPERATOR,
        access=AttrWriteType.READ_WRITE,
        fset="write_voltage",
    )
    def Voltage(self):
        return self.voltage

    @attribute(
        dtype=float,
        unit="l/s",
        format="%6.2f",
        display_level=DispLevel.EXPERT,
        access=AttrWriteType.READ_WRITE,
        fset="write_pump_size",
    )
    def PumpSize(self):
        return self.pump_size

    @attribute(
        dtype=float,
        unit="mbar",
        polling_period=3000,
        format="%1.2e",
        display_level=DispLevel.OPERATOR,
        access=AttrWriteType.READ,
    )
    def Pressure(self):
        return self.pressure

    @attribute(
        dtype=float,
        unit="A",
        format="%1.2e",
        display_level=DispLevel.OPERATOR,
        access=AttrWriteType.READ,
    )
    def Current(self):
        return self.current

    @attribute(dtype=str, display_level=DispLevel.OPERATOR, access=AttrWriteType.READ)
    def FirmwareVersion(self):
        return self.firmware_version

    @attribute(dtype=str, display_level=DispLevel.OPERATOR, access=AttrWriteType.READ)
    def Model(self):
        return self.spc_model

    @attribute(
        dtype=float,
        unit="mbar",
        format="%1.2e",
        display_level=DispLevel.EXPERT,
        access=AttrWriteType.READ_WRITE,
        fset="write_setpoint_on_pressure",
    )
    def SetPointOnPressure(self):
        return self.setpoint_on_pressure

    @attribute(
        dtype=float,
        unit="mbar",
        format="%1.2e",
        display_level=DispLevel.EXPERT,
        access=AttrWriteType.READ_WRITE,
        fset="write_setpoint_off_pressure",
    )
    def SetPointOffPressure(self):
        return self.setpoint_off_pressure

    @attribute(
        dtype=bool,
        display_level=DispLevel.EXPERT,
        access=AttrWriteType.READ_WRITE,
        fset="write_setpoint_enabled",
    )
    def SetPointEnabled(self):
        return self.setpoint_enabled

    @attribute(dtype=bool, display_level=DispLevel.EXPERT, access=AttrWriteType.READ)
    def SetPointActive(self):
        return self.setpoint_active

    # Proxy attributes
    @proxy_attribute(
        dtype=bool, access=AttrWriteType.READ, property_name="AlarmAttribute"
    )
    def Alarm(self, alarm):
        return alarm

    @proxy_attribute(
        dtype=bool,
        access=AttrWriteType.READ_WRITE,
        fset="write_bypass",
        property_name="BypassAttribute",
    )
    def Bypass(self, bypass):
        return bypass

    # Proxy commands
    @proxy_command(
        property_name="BypassAttribute",
        dtype_in=bool,
        display_level=DispLevel.EXPERT,
        write_attribute=True,
    )
    def WriteBypass(self, subcommand, arg):
        subcommand(arg)

    # Commands
    @command(display_level=DispLevel.OPERATOR)
    @DebugIt()
    def Start(self):
        try:
            self.spc.start_pump()
        except Exception as e:
            self.error_stream("Start: " + str(e))

    @command(display_level=DispLevel.OPERATOR)
    @DebugIt()
    def Stop(self):
        try:
            self.spc.stop_pump()
        except Exception as e:
            self.error_stream("Stop: " + str(e))

    @command(
        dtype_in=str,
        doc_in="Command",
        dtype_out=str,
        doc_out="Response to command",
        display_level=DispLevel.EXPERT,
    )
    def SendCommand(self, cmd):
        if self.spc is None:
            return
        try:
            argout = self.spc.command_inout(cmd)
            return argout
        except Exception as e:
            # socket.error, SPCError
            self.error_stream("SendCommand: " + str(e))

    @command(display_level=DispLevel.EXPERT)
    @DebugIt()
    def UpdateFirmware(self):
        if self.spc is None:
            return
        try:
            if self.TFTPServerIP:
                self.spc.set_tftp_server_ip(self.TFTPServerIP)
            self.spc.set_firmware_update()
            self.update_pending = True
        except Exception as e:
            # socket.error, SPCError
            self.error_stream("UpdateFirmware: " + str(e))

    @command(display_level=DispLevel.EXPERT, polling_period=2000)
    @DebugIt()
    def ReadValues(self):
        try:
            if not self.connected:
                self.spc.connect(self.Host, self.Port, self.connect_timeout_s)
                # enable SPCe command mode
                self.spc.command_inout("SPC 41 0")
                # report pressure in mBar
                self.spc.set_pressure_units("MBR")
                # read model and firmware version
                self.firmware_version = self.spc.get_firmware_version()
                self.spc_model = self.spc.get_model_number()
                self.connected = True
                self.update_pending = False

            self.spc_status = self.spc.get_status()
            if self.spc_status.upper() in [
                "RUNNING",
                "PUMP RUNNING",
                "STARTING",
                "PUMP STARTING",
                "STOPPING",
                "PUMP STOPPING",
            ]:
                self.current = self.spc.get_current()
                self.pressure = self.spc.get_pressure()
                self.voltage = self.spc.get_voltage()
            else:
                self.current = None
                self.pressure = None
                self.voltage = 0
            self.high_voltage = self.spc.get_high_voltage()
            self.pump_size = self.spc.get_pump_size()

            (
                self.setpoint_enabled,
                self.setpoint_on_pressure,
                self.setpoint_off_pressure,
                self.setpoint_active,
            ) = self.spc.get_setpoint()

            if self.connected:
                status = "Connection OK"
                if self.spc_status.upper() in ["STANDBY", "PUMP READY"]:
                    status += "\nPump is ready"
                    state = DevState.STANDBY
                elif self.spc_status.upper() in ["RUNNING", "PUMP RUNNING"]:
                    status += "\nPump is running"
                    # RT4371 Check for abnormal pressure
                    if self.pressure == 1e-11:
                        status += "\nAbnormal pressure"
                        status += "\nThis controller needs maintenance"
                        state = DevState.ALARM
                    elif self.setpoint_active:
                        status += "\nSet point is active"
                        state = DevState.ON
                    elif self.setpoint_enabled:
                        status += "\nSet point not reached"
                        state = DevState.ALARM
                    else:
                        status += "\nSet point disabled"
                        state = DevState.ALARM
                elif self.spc_status.upper() in ["STARTING", "PUMP STARTING"]:
                    status += "\nPump is starting"
                    state = DevState.MOVING
                elif self.spc_status.upper() in ["STOPPING", "PUMP STOPPING"]:
                    status += "\nPump is stopping"
                    state = DevState.MOVING
                elif self.spc_status.upper() in ["PUMP ERROR", "PUMP DISABLED"]:
                    status += "\nPump is disabled/interlocked"
                    state = DevState.ALARM
                else:
                    status += "\n" + self.spc_status
                    state = DevState.UNKNOWN
                if self.update_pending:
                    status += "\nA firmware upgrade is pending"
            else:
                state = DevState.FAULT
                if self.Host:
                    status = "Cannot connect to %s:%d\n" % (self.Host, self.Port)
                else:
                    status = '"Host" device property is not set'
                if self.spc_error:
                    status += self.spc_error
            self.set_state(state)
            self.set_status(status)

        except Exception as e:
            self.error_stream("ReadValues: " + str(e))
            self.spc.disconnect()
            self.spc_error = str(e)
            self.connected = False
            self.set_state(DevState.FAULT)
            self.set_status(str(e))

    # write methods
    def write_bypass(self, bypass):
        self.WriteBypass(bypass)

    def write_voltage(self, voltage):
        try:
            self.spc.set_high_voltage(voltage)
        except Exception as e:
            self.error_stream("Voltage: " + str(e))

    def write_pump_size(self, pump_size):
        try:
            self.spc.set_pump_size(pump_size)
        except Exception as e:
            self.error_stream("Pump size: " + str(e))

    def write_setpoint_on_pressure(self, setpoint_on_pressure):
        try:
            self.spc.set_setpoint(
                self.setpoint_enabled, setpoint_on_pressure, self.setpoint_off_pressure
            )
        except Exception as e:
            self.error_stream("Setpoint on pressure: " + str(e))

    def write_setpoint_off_pressure(self, setpoint_off_pressure):
        try:
            self.spc.set_setpoint(
                self.setpoint_enabled, self.setpoint_on_pressure, setpoint_off_pressure
            )
        except Exception as e:
            self.error_stream("Setpoint off pressure: " + str(e))

    def write_setpoint_enabled(self, setpoint_enabled):
        try:
            self.spc.set_setpoint(
                setpoint_enabled, self.setpoint_on_pressure, self.setpoint_off_pressure
            )
        except Exception as e:
            self.error_stream("Setpoint enabled: " + str(e))

    # other methods
    def delete_device(self):
        try:
            self.spc.disconnect()
        except Exception as e:
            self.error_stream(str(e))


# run server
run = GammaSPCe.run_server

if __name__ == "__main__":
    run()
