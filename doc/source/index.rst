.. GammaSPCe Documentation documentation master file, created by
   sphinx-quickstart on Wed May 23 13:07:29 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to GammaSPCe Documentation's documentation!
===================================================

This Device Server controls a Gamma Vacuum Small Pump Controller (SPC).
It communicates with the controller over ethernet via a TCP socket.

How to run
----------
After installation, there is only one script: `GammaSPCe`.
You can use it via:

.. code-block:: python

   GammaSPCe instance_name

Remember that Device Server instance has to registered in database previously.

Requirements
------------

- `setuptools`
- `pytango` >= 9.2.1

License
-------
This sample project is distributed under LGPLv3 (see `LICENSE` file).


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   properties
   commands
   attributes
