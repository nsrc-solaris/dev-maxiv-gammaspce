Commands list
==============


Commands for Operator Level:
----------------------------

* **Init** - This commands re-initialise a device keeping the same network connection. After an Init command executed on a device, it is not necessary for client to re-connect to the device. This command first calls the device delete_device() method and then execute its init_device() method.
* **State** - This command gets the device state (stored in its device_state data member) and returns it to the caller.
* **Status** - This command gets the device status (stored in its device_status data member) and returns it to the caller.
* **Start** - This command starts the pump (HV on mode)
* **Stop** - This command stops the pump (HV off mode)

Commands for Expert Level:
--------------------------

Those above plus:

* **UpdateFirmware** - This command starts the firmware update procedure from TFTP server
* **ReadValues** - This command reads all values and returns it to the caller
* **SendCommand** - Send command to controller. Possible command are listed below.


Available commands to send:
---------------------------

+------------+------------------------+------------------+
| Command    | Description            | Remarks          |
+============+========================+==================+
| SPC 01     | Get model number       | -                |
+------------+------------------------+------------------+
| SPC 02     | Get firmware version   | -                |
+------------+------------------------+------------------+
| SPC 07     | Master Reset           | -                |
+------------+------------------------+------------------+
| SPC FF     | Master Reset (legacy)  | -                |
+------------+------------------------+------------------+
| SPC 91     | Set Arc detect         | YES/NO           |
+------------+------------------------+------------------+
| SPC 92     | Get Arc detect         | -                |
+------------+------------------------+------------------+
| SPC 0A     | Read current           | -                |
+------------+------------------------+------------------+
| SPC 0B     | Read pressure          | -                |
+------------+------------------------+------------------+
| SPC 0C     | Read voltage           | -                |
+------------+------------------------+------------------+
| SPC 0D     | Get supply status      | -                |
+------------+------------------------+------------------+
| SPC 0E     | Set pressure units     | T/M/P            |
+------------+------------------------+------------------+
| SPC 11     | Get pump size          | -                |
+------------+------------------------+------------------+
| SPC 12     | Set pump size          | value (l/s)      |
+------------+------------------------+------------------+
| SPC 1D     | Get cal factor         | -                |
+------------+------------------------+------------------+
| SPC 1E     | Set cal factor         | n.nn             |
+------------+------------------------+------------------+
| SPC 33     | Set auto-restart       | YES/NO           |
+------------+------------------------+------------------+
| SPC 34     | Get auto-restart       | -                |
+------------+------------------------+------------------+
| SPC 37     | Start pump             | none or 1        |
+------------+------------------------+------------------+
| SPC 38     | Stop pump              | none or 1        |
+------------+------------------------+------------------+
| SPC 3C     | Get setpoint           | -                |
+------------+------------------------+------------------+
| SPC 3D     | Set setpoint           | *see manual*     |
+------------+------------------------+------------------+
| SPC 44     | Lock keypad            | -                |
+------------+------------------------+------------------+
| SPC 45     | Unlock keypad          | -                |
+------------+------------------------+------------------+
| SPC 50     | Get analog mode        | -                |
+------------+------------------------+------------------+
| SPC 51     | Set analog mode        | *see manual*     |
+------------+------------------------+------------------+
| SPC 61     | Is HV on               | -                |
+------------+------------------------+------------------+
| SPC 62     | Set serial address     | nnn              |
+------------+------------------------+------------------+
| SPC 68     | Set HV autorecovery    | *see manual*     |
+------------+------------------------+------------------+
| SPC 69     | Get HV autorecovery    | -                |
+------------+------------------------+------------------+
| SPC 8F     | Set firmware update    | -                |
+------------+------------------------+------------------+
| SPC D3     | Set comm mode          | *see manual*     |
+------------+------------------------+------------------+
| SPC D4     | Get comm mode          | -                |
+------------+------------------------+------------------+
| SPC 46     | Get/set serial comm    | *see manual*     |
+------------+------------------------+------------------+
| SPC 47     | Get/set ethernet IP    | none/X.X.X.X     |
+------------+------------------------+------------------+
| SPC 48     | Get/set ethernet mask  | none/X.X.X.X     |
+------------+------------------------+------------------+
| SPC 49     | Get/set ethernet gtwy  | none/X.X.X.X     |
+------------+------------------------+------------------+
| SPC 4A     | Get ethernet MAC       | -                |
+------------+------------------------+------------------+
| SPC 4B     | Set comm interface     | *see manual*     |
+------------+------------------------+------------------+
| SPC 4C     | Initiate FEA           | -                |
+------------+------------------------+------------------+
| SPC 4D     | Get FEA data           | *see manual*     |
+------------+------------------------+------------------+
| SPC 52     | Initiate hipot         | -                |
+------------+------------------------+------------------+
| SPC 53     | Get/set hipot target   | none/XXXX        |
+------------+------------------------+------------------+
| SPC 54     | Get/set foldback volts | none/XXXX        |
+------------+------------------------+------------------+
| SPC 55     | Get/set foldback pres  | none/XXXX        |
+------------+------------------------+------------------+

