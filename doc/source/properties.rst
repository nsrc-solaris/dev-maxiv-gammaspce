Properties list
===============

* **Host** - Host name or IP address (no default value)
* **Port** - Telnet port number (by default 23)
* **ReadTimeout** - Read timeout in milliseconds (by default 5000)
* **ConnectTimeout** - Connect timeout in millisecond (by default 1000)
* **TFTPServerIP** - IP address for TFTP server hosting firmware images (by default 192.168.130.90)
