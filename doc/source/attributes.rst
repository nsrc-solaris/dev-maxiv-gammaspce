Attributes list
===============

* **Voltage**
* **PumpSize**
* **Pressure**
* **Current**
* **FirmwareVersion**
* **Model**
* **SetPointOnPressure**
* **SetPointOffPressure**
* **SetPointEnabled**
* **SetPointActive**
