from setuptools import find_packages, setup

from gammaspce.Release import (
    __name__,
    __author__,
    __author_email__,
    __version__,
    __license__,
    __url__,
    __description__,
)

setup(
    name=__name__,
    author=__author__,
    author_email=__author_email__,
    version=__version__,
    license=__license__,
    description=__description__,
    url=__url__,
    packages=find_packages(),
    include_package_data=True,
    install_requires=["setuptools"],
    entry_points={"console_scripts": ["GammaSPCe = gammaspce.GammaSPCe:run"]},
)
